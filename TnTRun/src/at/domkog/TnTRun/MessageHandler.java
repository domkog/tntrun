package at.domkog.TnTRun;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class MessageHandler {

	public enum MessageType {
		
		INFO(ChatColor.GRAY + "[" + ChatColor.RED + "TnTRun" + ChatColor.GRAY + "] " + ChatColor.YELLOW),
		WARNING(ChatColor.GRAY + "[" + ChatColor.RED + "TnTRun" + ChatColor.GRAY + "] " + ChatColor.RED),
		ERROR(ChatColor.GRAY + "[" + ChatColor.RED + "TnTRun" + ChatColor.GRAY + "] " + ChatColor.DARK_RED),
		RAW(ChatColor.GRAY + "[" + ChatColor.RED + "TnTRun" + ChatColor.GRAY + "] " + ChatColor.DARK_RED);
		
		private String prefix;
		
		MessageType(String prefix) {
			this.prefix = prefix;
		}
		
		public String formatMessage(String message) {
			return this.prefix + message;
		}
		
	}
	
	public static void sendMessage(Player player, MessageType type, String message) {
		player.sendMessage(type.formatMessage(message));
	}
	
}
