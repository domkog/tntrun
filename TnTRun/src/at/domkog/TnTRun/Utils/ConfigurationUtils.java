package at.domkog.TnTRun.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

public class ConfigurationUtils {

	public static void saveLocation(FileConfiguration config, String path, Location loc) {
		config.set(path + ".world", loc.getWorld().getName());
		config.set(path + ".x", loc.getX());
		config.set(path + ".y", loc.getY());
		config.set(path + ".z", loc.getZ());
		config.set(path + ".pitch", loc.getPitch());
		config.set(path + ".yaw", loc.getYaw());
	}
	
	public static Location getLocation(FileConfiguration config, String path) {
		String name = "";
		double x = 0D, y = 0D, z = 0D;
		float pitch = 0.0f, yaw = 0.0f;
		
		name = config.getString(path + ".world");
		x = config.getDouble(path + ".x");
		y = config.getDouble(path + ".y");
		z = config.getDouble(path + ".z");
		pitch = (float) config.getDouble(path + ".pitch");
		yaw = (float) config.getDouble(path + ".yaw");
		
		if(name == null || name.equalsIgnoreCase("")) return null;
		
		return new Location(Bukkit.getWorld(name), x, y, z, yaw, pitch);
	}
	
}
