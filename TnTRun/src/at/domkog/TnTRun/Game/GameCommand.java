package at.domkog.TnTRun.Game;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import at.domkog.TnTRun.MessageHandler;
import at.domkog.TnTRun.TnTRun;
import at.domkog.TnTRun.MessageHandler.MessageType;

public class GameCommand implements CommandExecutor {

	private TnTRun pluginInstance;
	
	public GameCommand(TnTRun pluginInstance) {
		this.pluginInstance = pluginInstance;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(label.equalsIgnoreCase("game")) {
			if(!(sender instanceof Player)) {
				sender.sendMessage(" >> You must be a Player to use this Command!");
				return true;
			}
			Player player = (Player) sender;
			
			if(args.length == 0) {
				return true;
			}
			
			if(args[0].equalsIgnoreCase("setLobby")) {
				if(!player.hasPermission("TnTRun.Setup")) {
					MessageHandler.sendMessage(player, MessageType.WARNING, "You do not have the permission to perform this command!");
					return true;
				}
				this.pluginInstance.game.getGameConfiguration().setLobbySpawn(player.getLocation());
				MessageHandler.sendMessage(player, MessageType.INFO, "Lobby spawn successfully updated!");
				return true;
			}
		}
		return true;
	}

}
