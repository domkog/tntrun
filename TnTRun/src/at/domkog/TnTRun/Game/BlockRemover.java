package at.domkog.TnTRun.Game;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;

import at.domkog.TnTRun.TnTRun;

public class BlockRemover implements Runnable {

	private Block block;
	
	public BlockRemover(TnTRun pluginInstance, Block block) {
		this.block = block;
		Bukkit.getScheduler().scheduleSyncDelayedTask(pluginInstance, this, 7L);
	}

	@Override
	public void run() {
		this.block.setType(Material.AIR);
	}
	
}
