package at.domkog.TnTRun.Game;

import java.io.File;
import java.io.IOException;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import at.domkog.TnTRun.Utils.ConfigurationUtils;

public class GameConfiguration {

	public File configFile;
	public FileConfiguration config;
			
	private Location lobbySpawn;
	
	private int voteTime = 120;
	private Material voteItem = Material.PAPER;
	
	private int requieredPlayerStart = 2;
	
	public GameConfiguration(File configFile) {
		this.configFile = configFile;
		this.config = YamlConfiguration.loadConfiguration(configFile);
		this.setDefaultConfig();
		this.loadConfiguration();
	}

	public Location getLobbySpawn() {
		return lobbySpawn;
	}

	public void setLobbySpawn(Location lobbySpawn) {
		this.lobbySpawn = lobbySpawn;
		ConfigurationUtils.saveLocation(this.config, "lobbySpawn", this.lobbySpawn);
		this.saveConfig();
	}

	public int getVoteTime() {
		return voteTime;
	}

	public Material getVoteItem() {
		return voteItem;
	}

	public int getRequieredPlayersStart() {
		return this.requieredPlayerStart;
	}
	
	private void saveConfig() {
		try {
			this.config.save(this.configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void setDefaultConfig() {
		this.config.addDefault("lobbySpawn.world", "");
		this.config.addDefault("lobbySpawn.x", 0D);
		this.config.addDefault("lobbySpawn.y", 0D);
		this.config.addDefault("lobbySpawn.z", 0D);
		this.config.addDefault("lobbySpawn.pitch", 0f);
		this.config.addDefault("lobbySpawn.yaw", 0f);
		
		this.config.addDefault("voteTime", 120);
		this.config.addDefault("voteItem", Material.PAPER.toString());
		
		this.config.addDefault("requieredPlayers", 2);
		
		this.config.options().copyDefaults(true);
		
		this.saveConfig();
	}
	
	private void loadConfiguration() {
		this.lobbySpawn = ConfigurationUtils.getLocation(this.config, "lobbySpawn");
		
		this.voteTime = this.config.getInt("voteTime");
		this.voteItem = Material.valueOf(this.config.getString("voteItem"));
		
		this.requieredPlayerStart = this.config.getInt("requieredPlayers");
	}
	
}
