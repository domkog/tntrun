package at.domkog.TnTRun.Game;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import at.domkog.TnTRun.MessageHandler;
import at.domkog.TnTRun.MessageHandler.MessageType;
import at.domkog.TnTRun.TnTRun;
import at.domkog.TnTRun.Game.Map.Map;
import at.domkog.TnTRun.Game.Voting.GameVoting;

public class Game {

	private TnTRun pluginInstance;
	
	private GameState gameState;
	private GameConfiguration config;
	
	private GameVoting voting;
	private Map map;
	
	private ArrayList<String> playing = new ArrayList<String>();
	
	public Game(TnTRun pluginInstance) {
		this.pluginInstance = pluginInstance;
		this.gameState = GameState.VOTING;
		this.config = new GameConfiguration(new File(this.pluginInstance.getDataFolder(), "Game.yml"));
	}
	
	public void onGameStart(Map map) {
		this.map = map;
	}
	
	public void onGameEnd(Player player, HashMap<Block, Material> removed) {
		this.gameState = GameState.ENDING;
		for(Player onlinePlayer: Bukkit.getOnlinePlayers()) {
			onlinePlayer.playSound(onlinePlayer.getLocation(), Sound.FIREWORK_LAUNCH, 1f, 1f);
			MessageHandler.sendMessage(onlinePlayer, MessageType.INFO, ChatColor.GOLD + player.getName() + ChatColor.GREEN + " hat das Spiel gewonnen!");
			onlinePlayer.playSound(onlinePlayer.getLocation(), Sound.ORB_PICKUP, 1f, 1f);
		}
		new EndGameScheduler(this.pluginInstance, removed);
	}
	
	public Map getMap() {
		return this.map;
	}
	
	public ArrayList<String> getPlaying() {
		return this.playing;
	}
	
	public boolean isPlaying(Player player) {
		return this.playing.contains(player.getUniqueId().toString());
	}
	
	public void addPlaying(Player player) {
		this.playing.add(player.getUniqueId().toString());
	}
	
	public void removePlaying(Player player) {
		this.playing.remove(player.getUniqueId().toString());
	}
	
	public GameState getGameState() {
		return this.gameState;
	}
	
	public void setGameState(GameState state) {
		this.gameState = state;
	}
	
	public GameConfiguration getGameConfiguration() {
		return this.config;
	}
	
	public boolean hasGameVoting() {
		return this.voting != null;
	}
	
	public GameVoting getGameVoting() {
		return this.voting;
	}
	
	public void setGameVoting(GameVoting voting) {
		this.voting = voting;
	}
	
}
