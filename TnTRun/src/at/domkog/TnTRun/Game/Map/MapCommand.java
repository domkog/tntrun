package at.domkog.TnTRun.Game.Map;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import at.domkog.TnTRun.MessageHandler;
import at.domkog.TnTRun.MessageHandler.MessageType;
import at.domkog.TnTRun.TnTRun;

public class MapCommand implements CommandExecutor {

	private TnTRun pluginInstance;
	
	public MapCommand(TnTRun pluginInstance) {
		this.pluginInstance = pluginInstance;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(label.equalsIgnoreCase("Map")) {
			if(!(sender instanceof Player)) {
				sender.sendMessage(" >> You must be a Player to use this Command!");
				return true;
			}
			Player player = (Player) sender;
			if(!player.hasPermission("TnTRun.Setup")) {
				MessageHandler.sendMessage(player, MessageType.WARNING, "You do not have the permission to perform this command!");
				return true;
			}
			if(args.length == 0) {
				MessageHandler.sendMessage(player, MessageType.INFO, "Available commands:");
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.YELLOW + "/Map list");
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.YELLOW + "/Map info <map name>");
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.YELLOW + "/Map create <map name>");
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.YELLOW + "/Map delete <map name>");
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.YELLOW + "/Map setGameSpawn <map name>");
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.YELLOW + "/Map setSpectatorSpawn <map name>");
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.YELLOW + "/Map addMaterial <map name>");
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.YELLOW + "/Map removeMaterial <map name>");
				return true;
			}
			
			if(args[0].equalsIgnoreCase("list")) {
				StringBuilder maps = new StringBuilder();
				for(Map map: this.pluginInstance.mapManager.getMaps()) {
					if(maps.length() == 0) maps.append(map.getName());
					else maps.append(", " + map.getName());
				}
				MessageHandler.sendMessage(player, MessageType.INFO, "Loaded maps: " + maps.toString());
				return true;
				
				
			} else if(args[0].equalsIgnoreCase("info")) {
				if(args.length != 2) {
					MessageHandler.sendMessage(player, MessageType.WARNING, "Command usage: /map info <name>");
					return true;
				}
				if(!this.pluginInstance.mapManager.isMap(args[1])) {
					MessageHandler.sendMessage(player, MessageType.WARNING, args[1] + " is not a loaded map!");
					return true;
				}
				Map map = this.pluginInstance.mapManager.getMap(args[1]);
				Location gameSpawn = map.getGameSpawn();
				Location spectatorSpawn = map.getSpectatorSpawn();
				MessageHandler.sendMessage(player, MessageType.INFO, "Map information:");
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.YELLOW + "Name: " + ChatColor.GRAY + args[1]);
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.YELLOW + "Death height: " + ChatColor.GRAY + map.getDeathHeight());
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.YELLOW + "Game spawn: " + ChatColor.GRAY + "World: " + gameSpawn.getWorld().getName() + " x: " + gameSpawn.getX() + " y: " + gameSpawn.getY() + " z: " + gameSpawn.getZ());
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.YELLOW + "Spectator spawn: " + ChatColor.GRAY + "World: " + spectatorSpawn.getWorld().getName() + " x: " + spectatorSpawn.getX() + " y: " + spectatorSpawn.getY() + " z: " + spectatorSpawn.getZ());
				StringBuilder materials = new StringBuilder();
				for(Material mat: map.getDestroyableBlocks()) {
					if(materials.length() == 0) materials.append(mat.toString());
					else materials.append(", " + mat.toString());
				}
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.YELLOW + "Destroyable Blocks: " + ChatColor.GRAY + materials.toString());
				
				
			} else if(args[0].equalsIgnoreCase("create")) {
				if(args.length != 2) {
					MessageHandler.sendMessage(player, MessageType.WARNING, "Command usage: /map create <name>");
					return true;
				}
				if(this.pluginInstance.mapManager.isMap(args[1])) {
					MessageHandler.sendMessage(player, MessageType.WARNING, "Theres always a loaded map called " + args[1] + "!");
					return true;
				}
				this.pluginInstance.mapManager.createNewMap(args[1]);
				MessageHandler.sendMessage(player, MessageType.INFO, "Map " + args[1] + " successfully created!");
				return true;
				
				
			} else if(args[0].equalsIgnoreCase("delete")) {
				if(args.length != 2) {
					MessageHandler.sendMessage(player, MessageType.WARNING, "Command usage: /map delete <name>");
					return true;
				}
				if(!this.pluginInstance.mapManager.isMap(args[1])) {
					MessageHandler.sendMessage(player, MessageType.WARNING, args[1] + " is not a loaded map!");
					return true;
				}
				this.pluginInstance.mapManager.removeMap(args[1]);
				MessageHandler.sendMessage(player, MessageType.INFO, "Map " + args[1] + " successfully deleted!");
				return true;
				
				
			} else if(args[0].equalsIgnoreCase("setGameSpawn")) {
				if(args.length != 2) {
					MessageHandler.sendMessage(player, MessageType.WARNING, "Command usage: /map setGameSpawn <name>");
					return true;
				}
				if(!this.pluginInstance.mapManager.isMap(args[1])) {
					MessageHandler.sendMessage(player, MessageType.WARNING, args[1] + " is not a loaded map!");
					return true;
				}
				Map map = this.pluginInstance.mapManager.getMap(args[1]);
				map.setGameSpawn(player.getLocation());
				MessageHandler.sendMessage(player, MessageType.INFO, "GameSpawn successfully updated!");
				return true;
				
				
			} else if(args[0].equalsIgnoreCase("setSpectatorSpawn")) {
				if(args.length != 2) {
					MessageHandler.sendMessage(player, MessageType.WARNING, "Command usage: /map setSpectatorSpawn <name>");
					return true;
				}
				if(!this.pluginInstance.mapManager.isMap(args[1])) {
					MessageHandler.sendMessage(player, MessageType.WARNING, args[1] + " is not a loaded map!");
					return true;
				}
				Map map = this.pluginInstance.mapManager.getMap(args[1]);
				map.setSpectatorSpawn(player.getLocation());
				MessageHandler.sendMessage(player, MessageType.INFO, "SpectatorSpawn successfully updated!");
				return true;
			} else if(args[0].equalsIgnoreCase("addMaterial")) {
				if(args.length != 2) {
					MessageHandler.sendMessage(player, MessageType.WARNING, "Command usage: /map addMaterial <name>");
					return true;
				}
				if(!this.pluginInstance.mapManager.isMap(args[1])) {
					MessageHandler.sendMessage(player, MessageType.WARNING, args[1] + " is not a loaded map!");
					return true;
				}
				Map map = this.pluginInstance.mapManager.getMap(args[1]);
				Material mat = player.getLocation().subtract(0, 1, 0).getBlock().getType();
				if(!map.isDestroyableBlock(mat)) map.addDestroyableBlock(mat);
				MessageHandler.sendMessage(player, MessageType.INFO, mat.toString() + " successfully added as destroyable block!");
				return true;
				
				
			} else if(args[0].equalsIgnoreCase("removeMaterial")) {
				if(args.length != 2) {
					MessageHandler.sendMessage(player, MessageType.WARNING, "Command usage: /map removeMaterial <name>");
					return true;
				}
				if(!this.pluginInstance.mapManager.isMap(args[1])) {
					MessageHandler.sendMessage(player, MessageType.WARNING, args[1] + " is not a loaded map!");
					return true;
				}
				Map map = this.pluginInstance.mapManager.getMap(args[1]);
				Material mat = player.getLocation().subtract(0, 1, 0).getBlock().getType();
				if(map.isDestroyableBlock(mat)) map.removeDestroyableBlock(mat);
				MessageHandler.sendMessage(player, MessageType.INFO, mat.toString() + " successfully removed as destroyable block!");
				return true;
			} else if(args[0].equalsIgnoreCase("setDeathHeight")) {
				if(args.length != 3) {
					MessageHandler.sendMessage(player, MessageType.WARNING, "Command usage: /map setDeathHeight <name> <height>");
					return true;
				}
				if(!this.pluginInstance.mapManager.isMap(args[1])) {
					MessageHandler.sendMessage(player, MessageType.WARNING, args[1] + " is not a loaded map!");
					return true;
				}
				int height = 0;
				try {
					height = Integer.parseInt(args[2]);
				} catch(NumberFormatException e) {
					MessageHandler.sendMessage(player, MessageType.WARNING, "Height must be a number!");
					return true;
				}
				Map map = this.pluginInstance.mapManager.getMap(args[1]);
				map.setDeathHeight(height);
				MessageHandler.sendMessage(player, MessageType.INFO, "Death height successfully updated!");
				return true;
			}
		}
		return true;
	}

}
