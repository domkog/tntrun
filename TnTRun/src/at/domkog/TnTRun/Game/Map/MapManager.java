package at.domkog.TnTRun.Game.Map;

import java.io.File;
import java.util.ArrayList;

import at.domkog.TnTRun.TnTRun;

public class MapManager {

	private TnTRun pluginInstance;
	private File mapDir;

	private ArrayList<Map> maps = new ArrayList<Map>();
	
	public MapManager(TnTRun pluginInstance, File mapDir) {
		this.pluginInstance = pluginInstance;
		this.mapDir = mapDir;
		this.loadMaps();
	}
	
	public ArrayList<Map> getMaps() {
		return this.maps;
	}
	
	public boolean isMap(String name) {
		for(Map map: this.maps) {
			if(map.getName().equalsIgnoreCase(name)) return true;
		}
		return false;
	}
	
	public Map getMap(String name) {
		for(Map map: this.maps) {
			if(map.getName().equalsIgnoreCase(name)) return map;
		}
		return null;
	}
	
	public void createNewMap(String name) {
		Map map = new Map(new File(this.mapDir, name + ".yml"), name);
		this.maps.add(map);
	}
	
	public void removeMap(String name) {
		Map map = this.getMap(name);
		if(map != null) this.maps.remove(map);
		map.getFile().delete();
	}
	
	private void loadMaps() {
		pluginInstance.getLogger().info(">> Loading Maps...");
		if(!mapDir.exists()) mapDir.mkdirs();
		for(File file: this.mapDir.listFiles()) {
			if(this.isYamlFile(file)) {
				Map map = new Map(file);
				this.maps.add(map);
				pluginInstance.getLogger().info(">> Map " + map.getName() + " loaded!");
			}
		}
		pluginInstance.getLogger().info(">> " + this.maps.size() + " successfully loaded!");
	}
	
	private boolean isYamlFile(File file) {
		String path = file.getPath();
		int i = path.lastIndexOf('.');
		String fExtension = null;
		if (i >= 0) {
			fExtension = path.substring(i+1);
		}
		if(fExtension != null) {
			if(fExtension.equalsIgnoreCase("yml")) {
				return true;
			}
		}
		return false;
	}
	
}
