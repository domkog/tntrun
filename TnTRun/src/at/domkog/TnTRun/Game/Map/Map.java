package at.domkog.TnTRun.Game.Map;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import at.domkog.TnTRun.Utils.ConfigurationUtils;

public class Map {

	private String name;
	private int deathHeight;
	
	private Location gameSpawn;
	private Location spectatorSpawn;
	
	private ArrayList<Material> destroyableBlocks = new ArrayList<Material>();
	
	private File configFile;
	private FileConfiguration config;
	
	public Map(File config, String name) {
		this.configFile = config;
		this.name = name;
		this.config = YamlConfiguration.loadConfiguration(config);
		
		this.setDefaultConfig();
		
		this.config.set("name", this.name);
		
		this.saveConfig();
	}
	
	public Map(File config) {
		this.configFile = config;
		this.config = YamlConfiguration.loadConfiguration(config);
		
		this.setDefaultConfig();
		
		this.loadConfiguration();
	}

	public File getFile() {
		return this.configFile;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
		this.config.set("name", this.name);
		this.saveConfig();
	}
	
	public int getDeathHeight() {
		return this.deathHeight;
	}
	
	public void setDeathHeight(int height) {
		this.deathHeight = height;
		this.config.set("deathHeight", this.deathHeight);
		this.saveConfig();
	}
	
	public Location getGameSpawn() {
		return gameSpawn;
	}

	public void setGameSpawn(Location gameSpawn) {
		this.gameSpawn = gameSpawn;
		ConfigurationUtils.saveLocation(this.config, "gameSpawn", this.gameSpawn);
		this.saveConfig();
	}

	public Location getSpectatorSpawn() {
		return spectatorSpawn;
	}

	public void setSpectatorSpawn(Location spectatorSpawn) {
		this.spectatorSpawn = spectatorSpawn;
		ConfigurationUtils.saveLocation(this.config, "spectatorSpawn", this.spectatorSpawn);
		this.saveConfig();
	}
	
	public ArrayList<Material> getDestroyableBlocks() {
		return this.destroyableBlocks;
	}
	
	public void addDestroyableBlock(Material material) {
		this.destroyableBlocks.add(material);
		ArrayList<String> materialNames = new ArrayList<String>();
		
		for(Material mat: this.destroyableBlocks) materialNames.add(mat.toString());
		this.config.set("destroyableBlocks", materialNames);
		
		this.saveConfig();
	}
	
	public void removeDestroyableBlock(Material material) {
		this.destroyableBlocks.remove(material);
		
		ArrayList<String> materialNames = new ArrayList<String>();
		
		for(Material mat: this.destroyableBlocks) materialNames.add(mat.toString());
		this.config.set("destroyableBlocks", materialNames);
		
		this.saveConfig();
	}
	
	public boolean isDestroyableBlock(Material material) {
		return this.destroyableBlocks.contains(material);
	}
	
	public void saveConfig() {
		try {
			this.config.save(this.configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void setDefaultConfig() {
		this.config.addDefault("name", "");
		this.config.addDefault("deathHeight", 0);
		
		this.config.addDefault("gameSpawn.world", "");
		this.config.addDefault("gameSpawn.x", 0);
		this.config.addDefault("gameSpawn.y", 0);
		this.config.addDefault("gameSpawn.z", 0);
		this.config.addDefault("gameSpawn.pitch", 0f);
		this.config.addDefault("gameSpawn.yaw", 0f);
		
		this.config.addDefault("spectatorSpawn.world", "");
		this.config.addDefault("spectatorSpawn.x", 0D);
		this.config.addDefault("spectatorSpawn.y", 0D);
		this.config.addDefault("spectatorSpawn.z", 0D);
		this.config.addDefault("spectatorSpawn.pitch", 0f);
		this.config.addDefault("spectatorSpawn.yaw", 0f);
		
		this.config.addDefault("destroyableBlocks", new ArrayList<String>());
		
		this.config.options().copyDefaults(true);
		
		this.saveConfig();
	}
	
	private void loadConfiguration() {
		this.name = this.config.getString("name");
		this.deathHeight = this.config.getInt("deathHeight");
		
		this.gameSpawn = ConfigurationUtils.getLocation(this.config, "gameSpawn");
		this.spectatorSpawn = ConfigurationUtils.getLocation(this.config, "spectatorSpawn");
		
		for(String material: this.config.getStringList("destroyableBlocks")) {
			this.destroyableBlocks.add(Material.valueOf(material));
		}
	}
	
}
