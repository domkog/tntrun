package at.domkog.TnTRun.Game.Listeners;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import at.domkog.TnTRun.MessageHandler;
import at.domkog.TnTRun.TnTRun;
import at.domkog.TnTRun.Game.GameState;
import at.domkog.TnTRun.MessageHandler.MessageType;

public class PlayerQuitListener implements Listener {

	private TnTRun pluginInstance;
	
	public PlayerQuitListener(TnTRun pluginInstance) {
		this.pluginInstance = pluginInstance;
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		if(this.pluginInstance.game.getGameState() == GameState.VOTING) {
			if(!this.pluginInstance.game.hasGameVoting()) return;
			if(this.pluginInstance.game.getGameVoting().hasVoted(event.getPlayer())) {
				this.pluginInstance.game.getGameVoting().removeVote(event.getPlayer());
			}
		} else if(this.pluginInstance.game.getGameState() == GameState.INGAME) {
			if(!this.pluginInstance.game.isPlaying(event.getPlayer())) return;
			this.pluginInstance.game.removePlaying(event.getPlayer());
			for(Player player: Bukkit.getOnlinePlayers()) {
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.RED + event.getPlayer().getName() + ChatColor.GRAY + " hat das Spiel verlassen!");
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.RED + "" + this.pluginInstance.game.getPlaying().size() + ChatColor.GRAY + " Spieler verbleibend!");
			}
			if(this.pluginInstance.game.getPlaying().size() == 1) {
				this.pluginInstance.game.onGameEnd(Bukkit.getPlayer(UUID.fromString(this.pluginInstance.game.getPlaying().get(0))), this.pluginInstance.moveListener.removedBlocks);
			}
		}
	}
	
}
