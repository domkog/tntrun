package at.domkog.TnTRun.Game.Listeners;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import at.domkog.TnTRun.TnTRun;
import at.domkog.TnTRun.Game.GameState;
import at.domkog.TnTRun.Game.Voting.GameVoting;

public class PlayerJoinListener implements Listener {

	private TnTRun pluginInstance;
	
	public PlayerJoinListener(TnTRun pluginInstance) {
		this.pluginInstance = pluginInstance;
	}
	
	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event) {
		if(this.pluginInstance.game.getGameState() != GameState.VOTING) {
			event.setKickMessage(ChatColor.RED + "Das Spiel l�uft bereits!");
			event.setResult(Result.KICK_OTHER);
		}
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		event.getPlayer().getInventory().clear();
		if(this.pluginInstance.game.getGameState() == GameState.VOTING) {
			if(!this.pluginInstance.game.hasGameVoting()) {
				if(Bukkit.getOnlinePlayers().size() >= this.pluginInstance.game.getGameConfiguration().getRequieredPlayersStart()) {
					this.pluginInstance.game.setGameVoting(new GameVoting(this.pluginInstance));
					System.out.println("Starting Game Voting!");
				}
			} else {
				Player player = event.getPlayer();
				ItemStack voteItem = new ItemStack(this.pluginInstance.game.getGameConfiguration().getVoteItem());
				ItemMeta meta = voteItem.getItemMeta();
				meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Vote");
				voteItem.setItemMeta(meta);
				player.getInventory().addItem(voteItem);
			}
		}
		event.getPlayer().teleport(this.pluginInstance.game.getGameConfiguration().getLobbySpawn());
	}
	
}
