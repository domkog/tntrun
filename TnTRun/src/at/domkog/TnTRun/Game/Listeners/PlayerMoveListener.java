package at.domkog.TnTRun.Game.Listeners;

import java.util.HashMap;
import java.util.UUID;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import at.domkog.TnTRun.MessageHandler;
import at.domkog.TnTRun.TnTRun;
import at.domkog.TnTRun.Game.BlockRemover;
import at.domkog.TnTRun.Game.GameState;
import at.domkog.TnTRun.MessageHandler.MessageType;

public class PlayerMoveListener implements Listener {

	private TnTRun pluginInstance;
	
	public HashMap<Block, Material> removedBlocks = new HashMap<Block, Material>();
	
	public PlayerMoveListener(TnTRun pluginInstance) {
		this.pluginInstance = pluginInstance;
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		if(this.pluginInstance.game.getGameState() != GameState.INGAME) return;
		if(this.pluginInstance.game.getMap() == null) return;
		if(this.pluginInstance.game.isPlaying(player)) {
			if(e.getTo().getBlockY() <= this.pluginInstance.game.getMap().getDeathHeight()) {
				this.onPlayerDeath(e.getPlayer());
			}
			
			if(this.removedBlocks.containsKey(e.getFrom().getBlock().getRelative(BlockFace.DOWN))) return;
			if(this.pluginInstance.game.getMap().getDestroyableBlocks().contains(e.getFrom().getBlock().getRelative(BlockFace.DOWN).getType())) {
				this.removedBlocks.put(e.getFrom().getBlock().getRelative(BlockFace.DOWN), e.getFrom().getBlock().getRelative(BlockFace.DOWN).getType());
				new BlockRemover(this.pluginInstance, e.getFrom().getBlock().getRelative(BlockFace.DOWN));
			}
		}
	}
	
	private void onPlayerDeath(Player player) {
		this.pluginInstance.game.removePlaying(player);
		for(Player onlinePlayer: Bukkit.getOnlinePlayers()) {
			MessageHandler.sendMessage(onlinePlayer, MessageType.RAW, ChatColor.RED + player.getName() + ChatColor.GRAY + " ist ausgeschieden!");
			MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.RED + "" + this.pluginInstance.game.getPlaying().size() + ChatColor.GRAY + " Spieler verbleibend!");
			onlinePlayer.playSound(onlinePlayer.getLocation(), Sound.ANVIL_LAND, 1f, 1f);
		}
		if(this.pluginInstance.game.getPlaying().size() == 1) {
			Player winner = Bukkit.getPlayer(UUID.fromString(this.pluginInstance.game.getPlaying().get(0)));
			this.pluginInstance.game.onGameEnd(winner, this.removedBlocks);
		}
	}
	
}
