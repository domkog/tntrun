package at.domkog.TnTRun.Game.Voting;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import at.domkog.TnTRun.MessageHandler;
import at.domkog.TnTRun.MessageHandler.MessageType;
import at.domkog.TnTRun.TnTRun;

public class VoteCommand implements CommandExecutor {

	private TnTRun pluginInstance;
	
	public VoteCommand(TnTRun pluginInstance) {
		this.pluginInstance = pluginInstance;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(label.equalsIgnoreCase("vote")) {
			if(!(sender instanceof Player)) {
				sender.sendMessage(" >> You must be a Player to use this Command!");
				return true;
			}
			
			Player player = (Player) sender;
			
			if(args.length == 1) {
				if(!this.pluginInstance.game.hasGameVoting()) {
					MessageHandler.sendMessage(player, MessageType.WARNING, "Aktuell l�uft kein Map Voting!");
					return true;
				}
				if(!this.pluginInstance.game.getGameVoting().isAvailableMap(args[0])) {
					MessageHandler.sendMessage(player, MessageType.WARNING, "Die Map " + ChatColor.YELLOW + args[0] + ChatColor.RED + " ist in diesem Voting nicht verf�gbar!");
					return true;
				}
				this.pluginInstance.game.getGameVoting().addVote(this.pluginInstance.mapManager.getMap(args[0]), player);
				MessageHandler.sendMessage(player, MessageType.INFO, "Du hast f�r die Map " + args[0] + " abgestimmt! Votes: " + ChatColor.GOLD + this.pluginInstance.game.getGameVoting().getVotes(this.pluginInstance.mapManager.getMap(args[0])));
				return true;
			} else {
				MessageHandler.sendMessage(player, MessageType.WARNING, "Command usage: /vote <map name>");
				return true;
			}
			
		}
		return true;
	}

}
