package at.domkog.TnTRun.Game.Voting;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.entity.Player;

import at.domkog.TnTRun.TnTRun;
import at.domkog.TnTRun.Game.Map.Map;

public class GameVoting {

	private TnTRun pluginInstance;
	
	private VoteScheduler voting;
	private VoteScoreboard scoreboard;
	
	public HashMap<Map, Integer> mapVotes = new HashMap<Map, Integer>();
	public HashMap<String, Map> voted = new HashMap<String, Map>();
	
	public GameVoting(TnTRun pluginInstance) {
		this.pluginInstance = pluginInstance;
		this.selectMaps();
	}
	
	public TnTRun getPlugin() {
		return this.pluginInstance;
	}
	
	public VoteScheduler getVoteScheduler() {
		return this.voting;
	}
	
	public VoteScoreboard getVoteScoreboard() {
		return this.scoreboard;
	}

	public HashMap<Map, Integer> getMapChoices() {
		return this.mapVotes;
	}
	
	public boolean isAvailableMap(String name) {
		if(!this.pluginInstance.mapManager.isMap(name)) return false;
		return this.mapVotes.containsKey(this.pluginInstance.mapManager.getMap(name));
	}
	
	public int getVotes(Map map) {
		return this.mapVotes.get(map);
	}
	
	public boolean hasVoted(Player player) {
		return this.voted.containsKey(player.getUniqueId().toString());
	}
	
	public void addVote(Map map, Player player) {
		this.removeVote(player);
		this.mapVotes.put(map, (this.mapVotes.get(map) + 1));
		this.voted.put(player.getUniqueId().toString(), map);
		this.scoreboard.updateScoreboard();
	}
	
	public void removeVote(Player player) {
		if(this.voted.containsKey(player.getUniqueId().toString())) {
			this.removeVote(this.voted.get(player.getUniqueId().toString()), player);
		}
	}
	
	public void removeVote(Map map, Player player) {
		this.mapVotes.put(map, (this.mapVotes.get(map) - 1));
		this.voted.remove(player.getUniqueId().toString());
		this.scoreboard.updateScoreboard();
	}
	
	public Map getWinner() {
		Map winner = null;
		for(Map map: this.mapVotes.keySet()) {
			if(winner == null) winner = map;
			else if(this.mapVotes.get(map) >= this.mapVotes.get(winner)) winner = map;
		}
		return winner;
	}
	
	private void selectMaps() {
		if(this.pluginInstance.mapManager.getMaps().size() == 0) {
			this.pluginInstance.getLogger().warning(" >> There are no loaded maps. Add maps with /Map create <name> and restart the server!");
			return;
		}
		if(this.pluginInstance.mapManager.getMaps().size() <= 3) {
			for(Map map: this.pluginInstance.mapManager.getMaps()) this.mapVotes.put(map, 0);
		} else {
			ArrayList<Map> availableMaps = this.pluginInstance.mapManager.getMaps();
			for(int i = 0; i < 3; i++) {
				Map map = availableMaps.get(this.pluginInstance.rand.nextInt(availableMaps.size()));
				this.mapVotes.put(map, 0);
				availableMaps.remove(map);
			}
		}
		this.voting = new VoteScheduler(this);
		this.scoreboard = new VoteScoreboard(this);
	}
	
}
