package at.domkog.TnTRun.Game.Voting;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import at.domkog.TnTRun.Game.Map.Map;

public class VoteScoreboard {

	private GameVoting voting;
	
	private Scoreboard scoreboard;
	
	public VoteScoreboard(GameVoting voting) {
		this.voting = voting;
		this.initScoreboard();
	}
	
	public void removeScoreboard() {
		for(Player player: Bukkit.getOnlinePlayers()) player.setScoreboard(this.voting.getPlugin().getServer().getScoreboardManager().getNewScoreboard());
	}
	
	public void updateScoreboard() {
		for(Map map: this.voting.mapVotes.keySet()) {
			Score score = scoreboard.getObjective(DisplaySlot.SIDEBAR).getScore(map.getName());
			score.setScore(this.voting.mapVotes.get(map));
		}
		
		for(Player player: Bukkit.getOnlinePlayers()) player.setScoreboard(scoreboard);
	}
	
	private void initScoreboard() {
		
		ScoreboardManager sb = this.voting.getPlugin().getServer().getScoreboardManager();
		Scoreboard scoreboard = sb.getNewScoreboard();
		
		Objective o = scoreboard.registerNewObjective("Voting", "dummy");
		o.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		o.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Map voting");
		
		for(Map map: this.voting.mapVotes.keySet()) {
			Score score = o.getScore(map.getName());
			score.setScore(this.voting.mapVotes.get(map));
		}
		
		for(Player player: Bukkit.getOnlinePlayers()) player.setScoreboard(scoreboard);
	
		this.scoreboard = scoreboard;
	}
	
}
