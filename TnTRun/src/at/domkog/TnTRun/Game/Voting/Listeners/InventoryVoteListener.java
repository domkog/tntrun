package at.domkog.TnTRun.Game.Voting.Listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import at.domkog.TnTRun.TnTRun;
import at.domkog.TnTRun.Game.GameState;
import at.domkog.TnTRun.Game.Map.Map;

public class InventoryVoteListener implements Listener {

	private TnTRun pluginInstance;
	
	public InventoryVoteListener(TnTRun pluginInstance) {
		this.pluginInstance = pluginInstance;
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(this.pluginInstance.game.getGameState() != GameState.VOTING || !this.pluginInstance.game.hasGameVoting()) return;
		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			ItemStack item = e.getItem();
			if(!item.hasItemMeta()) return;
			if(!ChatColor.stripColor(item.getItemMeta().getDisplayName()).equalsIgnoreCase("Vote")) return;
			Inventory inv = Bukkit.createInventory(e.getPlayer(), 9, ChatColor.GOLD + "" + ChatColor.BOLD + "Map Voting");
			
			int counter = 0;
			
			for(Map map: this.pluginInstance.game.getGameVoting().getMapChoices().keySet()) {
				ItemStack mapItem = new ItemStack(Material.BOOK);
				ItemMeta mapMeta = mapItem.getItemMeta();
				mapMeta.setDisplayName(map.getName());
				mapItem.setItemMeta(mapMeta);
				int votes = this.pluginInstance.game.getGameVoting().getMapChoices().get(map);
				if(votes > 1) mapItem.setAmount(votes);
				inv.setItem(counter * 4, mapItem);
				counter++;
			}
			
			e.getPlayer().openInventory(inv);
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if(this.pluginInstance.game.getGameState() != GameState.VOTING || !this.pluginInstance.game.hasGameVoting()) {
			e.setCancelled(true);
			return;
		}
		
		Inventory inv = e.getClickedInventory();
		if(!ChatColor.stripColor(inv.getTitle()).equalsIgnoreCase("Map Voting")) {
			e.setCancelled(true);
			return;
		}
		
		ItemStack item = e.getCurrentItem();
		if(item == null || item.getType() == Material.AIR) {
			e.setCancelled(true);
			return;
		}
		if(!item.hasItemMeta()) {
			e.setCancelled(true);
			return;
		}
		
		Player player = (Player) e.getWhoClicked();
		player.performCommand("vote " + ChatColor.stripColor(item.getItemMeta().getDisplayName()));
		
		player.closeInventory();
		
		e.setCancelled(true);
	}
	
}
