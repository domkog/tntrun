package at.domkog.TnTRun.Game.Voting;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import at.domkog.TnTRun.MessageHandler;
import at.domkog.TnTRun.MessageHandler.MessageType;
import at.domkog.TnTRun.Game.PreGameScheduler;
import at.domkog.TnTRun.Utils.TitleManager;

public class VoteScheduler implements Runnable {

	private GameVoting voting;
	
	private int taskID;
	
	private int timer;
	
	public VoteScheduler(GameVoting voting) {
		System.out.println("New Vote Scheduler!");
		this.voting = voting;
		this.timer = voting.getPlugin().game.getGameConfiguration().getVoteTime();
		this.addVoteItem();
		this.taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(voting.getPlugin(), this, 0L, 20L);
	}

	private void addVoteItem() {
		for(Player player: Bukkit.getOnlinePlayers()) {
			player.getInventory().clear();
			ItemStack voteItem = new ItemStack(this.voting.getPlugin().game.getGameConfiguration().getVoteItem());
			ItemMeta meta = voteItem.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Vote");
			voteItem.setItemMeta(meta);
			player.getInventory().addItem(voteItem);
		}
	}
	
	private void onVoteEnd() {
		if(Bukkit.getOnlinePlayers().size() < this.voting.getPlugin().game.getGameConfiguration().getRequieredPlayersStart()) {
			this.voting.getPlugin().game.setGameVoting(null);
			for(Player player: Bukkit.getOnlinePlayers()) {
				MessageHandler.sendMessage(player, MessageType.INFO, "Das Voting ist ung�ltig da zu wenig Spieler f�r den Start online sind!");
				MessageHandler.sendMessage(player, MessageType.RAW, ChatColor.RED + "Spieler ben�tigt: " + this.voting.getPlugin().game.getGameConfiguration().getRequieredPlayersStart());
				player.getInventory().clear();
			}
			this.voting.getVoteScoreboard().removeScoreboard();
			return;
		} else {
			for(Player player: Bukkit.getOnlinePlayers()) {
				MessageHandler.sendMessage(player, MessageType.INFO, "Voting beendet! Map: " + this.voting.getWinner().getName());
				TitleManager.sendTitle(player, 5, 80, 5, ChatColor.GOLD + "" + ChatColor.BOLD + "Voting beendet!", ChatColor.YELLOW + "" + ChatColor.BOLD + "Map: " + this.voting.getWinner().getName());
			}
			this.voting.getVoteScoreboard().removeScoreboard();
		}
	}
	
	@Override
	public void run() {
		if(this.timer == -5) {
			new PreGameScheduler(this.voting, this.voting.getWinner());
			Bukkit.getScheduler().cancelTask(this.taskID);
		} else if(this.timer == 0) {
			this.onVoteEnd();
		} else {
			if(this.timer <= 5) {
				this.sendTimeMessage();
			} else if(this.timer <= 30) {
				if((this.timer % 10) == 0) {
					this.sendTimeMessage();
				}
			} else {
				if((this.timer % 30) == 0) {
					this.sendTimeMessage();
				}
			}
			
		}
		this.timer--;
	}
	
	private void sendTimeMessage() {
		if(this.timer < 0) return;
		for(Player player: Bukkit.getOnlinePlayers()) {
			MessageHandler.sendMessage(player, MessageType.INFO, "Voting endet in " + ChatColor.GOLD + this.timer + ChatColor.YELLOW + " Sekunde(n)!");
		}
	}
	
}
