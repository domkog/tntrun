package at.domkog.TnTRun.Game;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import at.domkog.TnTRun.MessageHandler;
import at.domkog.TnTRun.TnTRun;
import at.domkog.TnTRun.MessageHandler.MessageType;

public class EndGameScheduler implements Runnable {

	private TnTRun pluginInstance;
	
	private HashMap<Block, Material> removedBlocks;
	
	private int taskID;
	private int timer = 10;
	
	public EndGameScheduler(TnTRun pluginInstance, HashMap<Block, Material> removedBlocks) {
		this.pluginInstance = pluginInstance;
		this.removedBlocks = removedBlocks;
		this.taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(pluginInstance, this, 0L, 20L);
	}

	@Override
	public void run() {
		if(timer == 0) {
			for(Player player: Bukkit.getOnlinePlayers()) {
				player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1.0f, 1.0f);
				player.kickPlayer("Der Server startet nun neu!"); //Only for testing normally sending to the hub server
			}
			for(Block block: this.removedBlocks.keySet()) {
				block.setType(this.removedBlocks.get(block));
			}
			this.pluginInstance.game.setGameState(GameState.VOTING);
			Bukkit.getScheduler().cancelTask(this.taskID);
			return;
		}
		if(timer <= 3) {
			this.sendTimeMessage();
		} else {
			if((timer % 5) == 0) {
				this.sendTimeMessage();
			}
		}
		timer--;
	}

	private void sendTimeMessage() {
		for(Player player: Bukkit.getOnlinePlayers()) {
			MessageHandler.sendMessage(player, MessageType.INFO, "Server startet in " + ChatColor.GOLD + this.timer + ChatColor.YELLOW + " Sekunde(n) neu!");
		}
	}
	
}
