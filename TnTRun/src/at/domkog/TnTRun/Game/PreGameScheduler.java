package at.domkog.TnTRun.Game;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import at.domkog.TnTRun.MessageHandler;
import at.domkog.TnTRun.Game.Map.Map;
import at.domkog.TnTRun.Game.Voting.GameVoting;
import at.domkog.TnTRun.MessageHandler.MessageType;

public class PreGameScheduler implements Runnable {

	private GameVoting voting;
	private Map map;
	
	private int taskID;
	private int timer = 10;
	
	public PreGameScheduler(GameVoting voting, Map map) {
		this.voting = voting;
		this.map = map;
		this.taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(voting.getPlugin(), this, 0L, 20L);
		
		for(Player player: Bukkit.getOnlinePlayers()) {
			player.teleport(map.getGameSpawn());
			player.getInventory().clear();
			player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1f, 1f);
			voting.getPlugin().game.addPlaying(player);
		}
		voting.getPlugin().game.setGameState(GameState.INGAME);
	}

	@Override
	public void run() {
		if(Bukkit.getOnlinePlayers().size() <= 1) {
			Bukkit.getScheduler().cancelTask(this.taskID);
			return;
		}
		if(timer == 0) {
			this.voting.getPlugin().game.onGameStart(this.map);
			for(Player player: Bukkit.getOnlinePlayers()) player.playSound(player.getLocation(), Sound.ORB_PICKUP, 1.0f, 1.0f);
			Bukkit.getScheduler().cancelTask(this.taskID);
			return;
		}
		if(timer <= 3) {
			for(Player player: Bukkit.getOnlinePlayers()) player.playSound(player.getLocation(), Sound.NOTE_PLING, 1.0f, 1.0f);
			this.sendTimeMessage();
		} else {
			if((timer % 5) == 0) {
				this.sendTimeMessage();
			}
		}
		timer--;
	}

	private void sendTimeMessage() {
		for(Player player: Bukkit.getOnlinePlayers()) {
			MessageHandler.sendMessage(player, MessageType.INFO, "Das Spiel beginnt in " + ChatColor.GOLD + this.timer + ChatColor.YELLOW + " Sekunde(n)!");
		}
	}
	
}
