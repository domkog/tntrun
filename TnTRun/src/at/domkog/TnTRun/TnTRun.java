package at.domkog.TnTRun;

import java.io.File;
import java.util.Random;

import org.bukkit.plugin.java.JavaPlugin;

import at.domkog.TnTRun.Game.Game;
import at.domkog.TnTRun.Game.GameCommand;
import at.domkog.TnTRun.Game.Listeners.BlockedFeatures;
import at.domkog.TnTRun.Game.Listeners.PlayerJoinListener;
import at.domkog.TnTRun.Game.Listeners.PlayerMoveListener;
import at.domkog.TnTRun.Game.Listeners.PlayerQuitListener;
import at.domkog.TnTRun.Game.Map.MapCommand;
import at.domkog.TnTRun.Game.Map.MapManager;
import at.domkog.TnTRun.Game.Voting.VoteCommand;
import at.domkog.TnTRun.Game.Voting.Listeners.InventoryVoteListener;

public class TnTRun extends JavaPlugin {

	public MapManager mapManager;
	public Game game;
	
	public PlayerMoveListener moveListener;
	
	public Random rand = new Random();
	
	@Override
	public void onEnable() {
		this.mapManager = new MapManager(this, new File(this.getDataFolder(), "/Maps"));
		this.game = new Game(this);
		this.registerListeners();
		this.registerCommands();
		this.getLogger().info("TnTRun >> Plugin enabled!");
	}
	
	@Override
	public void onDisable() {
		this.getLogger().info("TnTRun >> Plugin enabled!");
	}
	
	private void registerListeners() {
		this.getServer().getPluginManager().registerEvents(new BlockedFeatures(), this);
		this.getServer().getPluginManager().registerEvents(new PlayerJoinListener(this), this);
		this.getServer().getPluginManager().registerEvents(new PlayerQuitListener(this), this);
		this.moveListener = new PlayerMoveListener(this);
		this.getServer().getPluginManager().registerEvents(this.moveListener, this);
		this.getServer().getPluginManager().registerEvents(new InventoryVoteListener(this), this);
	}
	
	private void registerCommands() {
		this.getCommand("Map").setExecutor(new MapCommand(this));
		this.getCommand("Game").setExecutor(new GameCommand(this));
		this.getCommand("Vote").setExecutor(new VoteCommand(this));
	}
	
}
